# Dataset

Home of the ground-truth dataset for Itsy-Bits.

### Optical Capture
Contains data optained with ground-truth optical tracking. That is, positions and orientation are measured with OptiTrack.


### App Capture
Contains data obtained with an Android app that instructs users to place and rotate objects on the screen.
Tablet data obtained with Samsung Galaxy Tab S2.
Data is cut to 15x27 images as the CNN is trained for this size (Nexus 5 capacitive image resolution)